# Technical Assessment - Chintan Patel

### Overview

- The automated tests were written in Java using the build automation tool Maven to simplify the code and hopefully simplify executing the tests for you.
- I used some libraries to simplify the code which can be seen in the pom.xml file.

### Executing Tests

1. `git clone https://Chintan-Patel@bitbucket.org/Chintan-Patel/assurityassessment.git`
2. Navigate to top level directory of `assurityassessment` where the `pom.xml` file is contained
3. To execute the tests run: `mvn clean test-compile test`. This should compile and execute the unit tests
4. If this doesn't work due to some reason (perhaps not having Maven or anything else) you can import the project into IntelliJ or Eclipse as a Maven project. From there you can run APIEndpointTest.java. 

### Expected Output:
```

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running assurity.assessment.APIEndpointTest
Tests run: 3, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.329 sec

Results :

Tests run: 3, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
```

### Other

- Test sources are located in src/test/java/assurity/assessment/APIEndpointTest.java
- Main sources are located in src/main/java/assurity/assessment/APICaller.java
