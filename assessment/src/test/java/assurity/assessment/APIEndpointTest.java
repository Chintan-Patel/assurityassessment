package assurity.assessment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class APIEndpointTest {
	private static JsonElement jsonElement;

	@Test
	public void testNameIsBadges() {
		final String expected = "Badges";
		final String actual = jsonElement.getAsJsonObject().get("Name").getAsString();
		assertEquals(expected, actual);
	}

	@Test
	public void testCanListClassifiedsIsFalse() {
		final Boolean expected = false;
		final Boolean actual = jsonElement.getAsJsonObject().get("CanListClassifieds").getAsBoolean();
		assertEquals(expected, actual);
	}

	@Test
	public void testCharityPlunketExists() {
		final String expectedDescription = "Plunket";
		final String expectedTagLineSubstring = "well child health services";
		// Get an array of charities as JSON objects
		final JsonArray charities = jsonElement.getAsJsonObject().get("Charities").getAsJsonArray();

		// Get the plunket charity JSON Object
		final JsonObject plunketCharity = arrayToStream(charities)
				.map(JsonObject.class::cast)
				.filter(element -> expectedDescription.equals(element.get("Description").getAsString()))
				.findFirst()
				.orElse(null);

		final String actualDescription = plunketCharity.get("Description").getAsString();
		final String actualTaglLine = plunketCharity.get("Tagline").getAsString();

		assertEquals(expectedDescription, actualDescription);
		assertTrue(actualTaglLine.contains(expectedTagLineSubstring));
	}

	@BeforeClass
	public static void setup() {
		APICaller connector = new APICaller();
		try {
			final String endpoint = "https://api.tmsandbox.co.nz/v1/Categories/6328/Details.json?catalogue=false";
			JsonElement response = connector.connect(new URL(endpoint));
			jsonElement = response;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		assert (jsonElement != null);
	}

	/**
	 * Helper method to allow stream functions to be used on JsonArray's.
	 *
	 * @param array
	 * @return
	 */
	private static Stream<JsonElement> arrayToStream(JsonArray array) {
		return StreamSupport.stream(array.spliterator(), false);
	}
}
