package assurity.assessment;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;


/**
 * This class is responsible for performing a GET request for a given URL.
 * The GET response returns InputStream which is parsed into a JSON object
 * @author Chintan Patel
 *
 */
public class APICaller {

	/**
	 * Performs a GET request to retrieve information from the given URL
	 * @param url
	 * @return -- JSON Object
	 * @throws IOException
	 */
	public JsonElement connect(URL url) throws IOException {
		CloseableHttpClient httpclient = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build(); // adds HTTP REDIRECT support to GET and POST methods
		try {
			HttpGet get = new HttpGet(url.toURI());
			return httpclient.execute(get, new JsonResponseHandler());
		} catch (URISyntaxException e) {
			throw new IOException(e);
		} finally {
			httpclient.close();
		}
	}

	static class JsonResponseHandler implements ResponseHandler<JsonElement> {
		public JsonElement handleResponse(HttpResponse response) throws IOException {
			//Parse the response into a JSON Object
			InputStream inputStream = response.getEntity().getContent();
			JsonElement element = new JsonParser().parse(new InputStreamReader(inputStream, "UTF-8"));
			return element;
		}
	}
}
